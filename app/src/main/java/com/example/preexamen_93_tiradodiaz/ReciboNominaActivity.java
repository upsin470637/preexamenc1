package com.example.preexamen_93_tiradodiaz;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.view.WindowInsets;
import androidx.appcompat.app.AppCompatActivity;

public class ReciboNominaActivity extends AppCompatActivity {

    private EditText numRecibo;
    private EditText lblnombre;
    private EditText hrTrabajadas;
    private EditText hrExtras;
    private RadioGroup puesto;
    private EditText subtotal;
    private EditText impuesto;
    private EditText totalPag;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R) {
            getWindow().setDecorFitsSystemWindows(false);
            getWindow().getInsetsController().hide(WindowInsets.Type.statusBars());
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }

        numRecibo = findViewById(R.id.numRecibo);
        lblnombre = findViewById(R.id.lblnombre);
        hrTrabajadas = findViewById(R.id.hrTrabajadas);
        hrExtras = findViewById(R.id.hrExtras);
        puesto = findViewById(R.id.puesto);
        subtotal = findViewById(R.id.subtotal);
        impuesto = findViewById(R.id.impuesto);
        totalPag = findViewById(R.id.totalPag);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        // Obtener los extras del Intent
        Intent intent = getIntent();
        String nombre = intent.getStringExtra("NOMBRE_TRABAJADOR");

        // Configurar el nombre en el campo correspondiente
        lblnombre.setText(nombre);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularPago();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent regresarIntent = new Intent(ReciboNominaActivity.this, MainActivity.class);
                startActivity(regresarIntent);
            }
        });
    }

    private void calcularPago() {
        String hrsTrabajadasStr = hrTrabajadas.getText().toString();
        String hrsExtrasStr = hrExtras.getText().toString();

        if (hrsTrabajadasStr.isEmpty() || hrsExtrasStr.isEmpty()) {
            Toast.makeText(this, "Por favor ingrese todas las horas", Toast.LENGTH_SHORT).show();
            return;
        }

        int hrsTrabajadas = Integer.parseInt(hrsTrabajadasStr);
        int hrsExtras = Integer.parseInt(hrsExtrasStr);

        double pagoBase = 200;
        double pagoPorHora = 0;

        int checkedRadioButtonId = puesto.getCheckedRadioButtonId();
        if (checkedRadioButtonId != -1) {
            RadioButton selectedRadioButton = findViewById(checkedRadioButtonId);
            String puestoTag = selectedRadioButton.getTag().toString();

            switch (puestoTag) {
                case "1":
                    pagoPorHora = pagoBase * 1.20;
                    break;
                case "2":
                    pagoPorHora = pagoBase * 1.50;
                    break;
                case "3":
                    pagoPorHora = pagoBase * 2.00;
                    break;
            }
        } else {
            Toast.makeText(this, "Por favor seleccione un puesto", Toast.LENGTH_SHORT).show();
            return;
        }

        double subtotalCalculado = (hrsTrabajadas * pagoPorHora) + (hrsExtras * pagoPorHora * 2);
        double impuestoCalculado = subtotalCalculado * 0.16;
        double totalCalculado = subtotalCalculado - impuestoCalculado;

        subtotal.setText(String.format("%.2f", subtotalCalculado));
        impuesto.setText(String.format("%.2f", impuestoCalculado));
        totalPag.setText(String.format("%.2f", totalCalculado));
    }

    private void limpiarCampos() {
        numRecibo.setText("");
        lblnombre.setText(""); // Limpiar el campo de nombre también
        hrTrabajadas.setText("");
        hrExtras.setText("");
        puesto.clearCheck();
        subtotal.setText("");
        impuesto.setText("");
        totalPag.setText("");
    }
}
